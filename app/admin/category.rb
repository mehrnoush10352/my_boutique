ActiveAdmin.register Category do
  menu parent: 'boutique'
  permit_params :id, :title, :image, :is_active, :url
  actions :all, except: [:show]

  filter :id
  filter :title
  filter :is_active
  filter :url
  index do
    selectable_column
    column :id
    column :title
    column :is_active
    column :url
    column :image, sortable: :image_file_name do |firmware|
      link_to firmware.image_file_name, firmware.image.url
    end
    actions
  end
  form do |f|
    f.inputs do
      f.input :title
      f.input :is_active
      f.input :url
      f.input :image, as: :file, hint:
        if f.object.image?
          image_tag(f.object.image.url(:med))
        else
          content_tag(:span, 'Upload JPG/PNG/GIF image')
        end
    end
    f.actions
  end

  controller do
    def update
      update! do |format|
        format.html { redirect_to edit_resource_path(resource) }
      end
    end
  end
end
