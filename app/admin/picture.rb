ActiveAdmin.register Picture do
  menu parent: 'boutique'
  permit_params :id, :title, :enable, :products_id, :file
  actions :all, except: [:show]

  filter :id
  filter :title
  filter :enable
  filter :product
  index do
    selectable_column
    column :id
    column :title
    column :enable
    column :product
    column :file, sortable: :file_file_name do |firmware|
      link_to firmware.file_file_name, firmware.file.url
    end
    actions
  end
  form do |f|
    f.inputs do
      f.input :title
      f.input :product
      f.input :enable
      f.input :file, as: :file, hint:
        if f.object.file?
          image_tag(f.object.file.url(:med))
        else
          content_tag(:span, 'Upload JPG/PNG/GIF image')
        end
      actions
    end
  end
  controller do
    def update
      update! do |format|
        format.html { redirect_to edit_resource_path(resource) }
      end
    end
  end

end
