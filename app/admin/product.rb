ActiveAdmin.register Product do
  menu parent: 'boutique'
  actions :all, except: [:show]
  permit_params :id, :categories_id, :title, :price, :discount, :image, :url,
    :features, :is_active, :description, :count, :enable

  filter :id
  filter :title
  filter :category
  filter :is_active
  index do
    selectable_column
    column :id
    column :title
    column :category
    column :is_active
    column :image, sortable: :image_file_name do |firmware|
      link_to firmware.image_file_name, firmware.image.url
    end
    column :count do |product|
      product.inventory.count if product.inventory.present?
    end
    actions
  end
  form do |f|
    f.inputs do
      f.input :title
      f.input :category
      f.input :price
      f.input :discount
      f.input :is_active
      f.input :features, as: :text
      f.input :url
      f.input :image, as: :file, hint:
        if f.object.image?
          image_tag(f.object.image.url(:med))
        else
          content_tag(:span, 'Upload JPG/PNG/GIF image')
        end
      f.input :description
    end
    actions
  end
  controller do
    def update
      update! do |format|
        format.html { redirect_to edit_resource_path(resource) }
      end
    end
  end
end
