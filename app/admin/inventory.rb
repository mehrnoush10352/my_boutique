ActiveAdmin.register Inventory do
  menu parent: 'boutique'
  permit_params :id, :count, :enable, :products_id
  actions :all, except: [:show]

  filter :id
  filter :count
  filter :products_id
  filter :enable

  index do
    selectable_column
    column :id
    column :product
    column :count
    column :enable
    actions
  end


end
