ActiveAdmin.register Customer do
  menu parent: 'customer'
  permit_params :id, :username, :password, :password_confirmation, :email, :given_name, :surname,
    :phone_number, :address, :is_active
  actions :all, except: [:show]
  batch_action :send_email do |ids|
    SendEmailWorker.perform_async(ids)
    redirect_to collection_path, alert: "Email has been send"
  end
  filter :id
  filter :username
  filter :email
  index do
    selectable_column
    column :id
    column :username
    column :email
    column :is_active
    actions
  end
  form do |f|
    f.inputs do
      f.input :given_name
      f.input :surname
      f.input :username
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :is_active
      f.input :phone_number
      f.input :address
      actions
    end
  end
  controller do
    def update
      update! do |format|
        format.html { redirect_to edit_resource_path(resource) }
      end
    end
  end
end
