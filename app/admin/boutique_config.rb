ActiveAdmin.register BoutiqueConfig do
  menu parent: 'boutique'
  permit_params :id, :name, :values
  actions :all, except: [:show]
  index do
    selectable_column
    id_column
    column :name
    column :values
    column :created_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :values, as: :jsonb
    end
    f.actions
  end

  controller do
    def update
      update! do |format|
        format.html { redirect_to edit_resource_path(resource) }
      end
    end
  end
end
