class IndexWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'indexer', retry: false

  Logger = Sidekiq.logger.level == Logger::DEBUG ? Sidekiq.logger : nil
  config = Rails.configuration.elastic
  Client = Elasticsearch::Client.new host: config[:host], logger: Logger

  def perform(operation, record_id, klass)
    logger.debug [operation, "ID: #{record_id}"]

    case operation.to_s
    when /index/
      record = record = klass.constantize.find(record_id)
      Client.index(
        index: ActiveSupport::Inflector.pluralize(klass.downcase),
        type: klass.downcase,
        id: record.id,
        body: record.__elasticsearch__.as_indexed_json
      )
    when /delete/
      Client.delete(
        index: ActiveSupport::Inflector.pluralize(klass.downcase),
        type: klass.downcase,
        id: record.id,
        body: record.__elasticsearch__.as_indexed_json
      )
    else raise ArgumentError, "Unknown operation '#{operation}'"
    end
  end
end
