
# product's category
class Category < ApplicationRecord
  include FactoryIndexer

  has_attached_file :image, styles: {
    small: '64x64',
    med: '100x100',
    large: '200x200'
  }
  validates_attachment :image, content_type: {
    content_type: %w[image/jpeg image/jpg image/png image/gif]
  }
  validates :title, presence: true
  validates :is_active, inclusion: { in: [true, false] }
end
