# All the config we use for boutique
class BoutiqueConfig < ApplicationRecord
  validates :name, presence: true
  validate :json_format

  protected

    def json_format
      errors[:base] << 'not in json format' unless JSON.json?(values)
    end
end
