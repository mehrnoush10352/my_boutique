module FactoryIndexer
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    after_save { IndexWorker.perform_async('index', id, self.class.to_s) }
    after_destroy { IndexWorker.perform_async('delete', id, self.class.to_s) }

    after_commit on: [:create] do
      __elasticsearch__.index_document if self.is_active?
    end

    after_commit on: [:update] do
      __elasticsearch__.update_document if self.is_active?
    end

    after_commit on: [:destroy] do
      __elasticsearch__.delete_document if self.is_active?
    end


    def self.search(query)
      __elasticsearch__.search(query)
    end
  end
end
