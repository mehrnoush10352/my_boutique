class Inventory < ApplicationRecord
  belongs_to :product, foreign_key: :products_id
  validates :count, numericality: { only_intger: true }
  validates :product, presence: true
  validates :enable, inclusion: { in: [true, false] }
end
