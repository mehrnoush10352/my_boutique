# Product class
class Product < ApplicationRecord
  include FactoryIndexer
  belongs_to :category, foreign_key: :categories_id
  has_many :pictures, foreign_key: :products_id
  has_one :inventory, foreign_key: :products_id
  validates :title, presence: true
  has_attached_file :image, styles: {
    small: '64x64',
    med: '100x100',
    large: '200x200'
  }
  validates_attachment :image, content_type: {
    content_type: %w[image/jpeg image/jpg image/png image/gif]
  }
  validates_numericality_of  :price
  validates_numericality_of  :discount
  validates :category, presence: true
  validates :is_active, inclusion: { in: [true, false] }
  validate :json_format
  validates :url, uniqueness: true, allow_blank: true

  protected

    def json_format
      return if features.blank?
      errors[:base] << 'not in json format' unless JSON.json?(features)
    end
end
