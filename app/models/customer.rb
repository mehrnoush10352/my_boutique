# customer class
class Customer < ApplicationRecord
  include FactoryIndexer

  validates :username, uniqueness: true, presence: true
  validates :email, uniqueness: true, presence: true
  validates_format_of :email, with: Devise.email_regexp
  validates_format_of :password, with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{6,}\Z/
  validates :password, presence: true
  validates :password, confirmation: { case_sensitive: true }
  validates_format_of :phone_number, with: /\A09[0-9]{9}\Z/i, allow_blank: true
  validates_length_of :username, minimum: 4, maximum: 32
  validates :is_active, inclusion: { in: [true, false] }
end
