class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order
  validates :qty, numericality: { only_integer: true }, presence: true
  validates :price, presence: true
  validates :total_price, presence: true
  validates :shipping, inclusion: { in: [true, false] }, allow_blank: true

  private
    def is_total_price_valid?
      return self.total_price == (self.price  * self.qty)
    end
end
