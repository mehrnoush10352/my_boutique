class Order < ApplicationRecord
  belongs_to :customer, optional: true
  validates :status, inclusion: { in: [true, false] }
  validates :grand_total, numericality: true
  validates :subtotal, numericality: true
  validates :discount, numericality: true
  validates :shipping, inclusion: { in: [true, false] }
  validates_format_of :customer_email, with: Devise.email_regexp
  validates :subtotal_with_discount, numericality: true
  validates :shipping_amount, numericality: true, presence: true, if: :is_shipped?

  private
    def is_shipped?
      self.shipping == true
    end
end
