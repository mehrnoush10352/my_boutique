# all of needed pictures belongs to this class
class Picture < ApplicationRecord
  belongs_to :product, foreign_key: :products_id
  has_attached_file :file, styles: {
    small: '64x64',
    med: '100x100',
    large: '200x200'
  }
  validates_attachment :file, content_type: {
    content_type: %w[image/jpeg image/jpg image/png image/gif]
  }
  validates :title, presence: true
  validates :product, presence: true
  validates :enable, inclusion: { in: [true, false] }
end
