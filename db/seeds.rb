# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created
# alongside the database with db:setup).
#
# Examples:
#
# movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
  AdminUser.create!(
    email: 'admin@example.com',
    password: 'password',
    password_confirmation: 'password'
  )

  300.times do
    Customer.create(
      username: Faker::Name.unique.name,
      email: Faker::Internet.unique.email,
      is_active: true,
      password: 'a123b456',
      address: Faker::Address.full_address
    )
  end
  category = Category.create(
    title: 'Super hero T-shirt',
    is_active: true
  )
  100.times do
    Product.create(
      title: Faker::Superhero.name,
      is_active: true,
      price: rand(1000..50000),
      discount: rand(500..1500),
      category: category
    )
  end
  category = Category.create(
    title: 'dress',
    is_active: true
  )
  clothes_type = ['top', 'skirt', 'dress', 'shoes']
  200.times do

    Product.create(
      title: Faker::Color.color_name + ' ' + clothes_type.sample,
      is_active: true,
      price: rand(1000..50000),
      discount: rand(500..1500),
      category: category,
      description: Faker::Friends.quote
    )
  end
end
