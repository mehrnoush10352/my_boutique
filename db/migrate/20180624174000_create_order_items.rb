class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.references :order
      t.references :product
      t.integer :qty
      t.float :price
      t.float :total_price
      t.boolean :shipping
      t.timestamps
    end
  end
end
