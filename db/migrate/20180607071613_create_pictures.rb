class CreatePictures < ActiveRecord::Migration[5.1]
  def change
    create_table :pictures do |t|
      t.string :title
      t.boolean :enable
      t.attachment :file
      t.references :products
      t.timestamps
    end
  end
end
