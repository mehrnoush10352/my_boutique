class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :username
      t.string :given_name
      t.string :surname
      t.string :password
      t.string :phone_number
      t.text :address
      t.string :email
      t.boolean :is_active
      t.timestamps
    end
    add_index :customers, :username, unique: true
    add_index :customers, :email, unique: true
  end
end
