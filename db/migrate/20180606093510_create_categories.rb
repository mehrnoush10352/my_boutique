class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :title
      t.boolean :is_active
      t.attachment :image
      t.string :url

      t.timestamps
    end
  end
end
