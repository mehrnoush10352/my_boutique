class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :title
      t.attachment :image
      t.float :price, default: 0
      t.float :discount, default: 0
      t.jsonb :features
      t.boolean :is_active, default: false
      t.string :url
      t.references :categories
      t.timestamps
    end
  end
end
