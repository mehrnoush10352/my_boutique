class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.boolean :status
      t.float :grand_total
      t.float :subtotal
      t.float :discount
      t.string :customer_email
      t.boolean :shipping
      t.float :shipping_amount
      t.float :subtotal_with_discount
      t.timestamps
    end
    add_index :orders, :customer_email
    add_reference :orders, :customer, index: true, foreign_key: true
  end
end
