class CreateInventories < ActiveRecord::Migration[5.1]
  def change
    create_table :inventories do |t|
      t.integer :count
      t.references :products
      t.boolean :enable
      t.timestamps
    end
  end
end
