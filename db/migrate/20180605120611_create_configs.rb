class CreateConfigs < ActiveRecord::Migration[5.1]
  def change
    create_table :boutique_configs do |t|
      t.string :name
      t.jsonb :values
      t.timestamps
    end
  end
end
