FactoryBot.define do
  factory :inventory do
    association :product, factory: :product, strategy: :build
    count 5
    enable true
    trait :invalid_count do
      count 'something else'
    end
    trait :invalid_status do
      enable nil
    end
  end
end
