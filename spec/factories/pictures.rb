FactoryBot.define do
  factory :picture do
    title 'picture 1'
    enable true
    association :product, factory: :product, strategy: :build
    trait :invalid_title do
      title nil
    end
    trait :invalid_enable do
      enable nil
    end
  end
end
