FactoryBot.define do
  factory :order_item do
    association :product, factory: :product,  strategy: build
    association :order, factory: :order, strategy: build
    qty 5
    price 1000
    total_price 5000
    shipping true
    trait :empty_qty do
      qty nil
    end
    trait :invalid_qty do
      qty 'five'
    end
    trait :empty_price do
      price nil
    end
    trait :empty_total_price do
      total_priice nil
    end
    trait :invalid_total_price do
      total_price 6000
    end
    trait :invalid_shipping do
      shipping 'it is shipping'
    end
  end
end
