FactoryBot.define do
  factory :customer do
    username 'mehrnoush'
    email 'mehrnoush@gmail.com'
    password  'a123b456'
    is_active true
    given_name 'mehrnoush'
    surname 'akbari'
    phone_number '09198647878'
    address 'some where nice'
    trait :invalid_username do
      username nil
    end
    trait :empty_email do
      email nil
    end
    trait :invalid_email do
      email 'testsomething'
    end
    trait :empty_password do
      password nil
    end
    trait :invalid_password do
      password '123456'
    end
    trait :invalid_status do
      is_active nil
    end
    trait :invalid_phone_number do
      phone_number '2345666'
    end
  end
end
