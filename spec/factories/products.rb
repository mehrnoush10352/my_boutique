FactoryBot.define do
  factory :product do
    my_attributes = {
      size: [36, 38, 40],
      color: %w[red green blue]
    }.to_json
    title 'black leather shoes'
    is_active true
    price 15000
    discount 300
    features my_attributes
    association :category, factory: :category, strategy: :build
    trait :invalid_title do
      title nil
    end
    trait :invalid_is_active do
      is_active nil
    end
    trait :invalid_price do
      price 'something'
    end
    trait :invalid_discount do
      discount 'it is 555555555'
    end
    trait :invalid_features do
      features 'color = red'
    end
  end
end
