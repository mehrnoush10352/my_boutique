FactoryBot.define do
  factory :category do
    title 'shoe'
    is_active true
    trait :invalid_title do
      title ''
    end
    trait :invalid_is_active do
      is_active nil
    end
  end
end
