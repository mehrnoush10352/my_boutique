FactoryBot.define do
  factory :boutique_config do
    name 'brand'
    values %w[versace nike puma zara].to_json
    trait :empty_name do
      name ''
    end
    trait :invalid_values do
      values 'some data'
    end
  end
end
