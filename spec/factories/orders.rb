FactoryBot.define do
  factory :order do
    status true
    grand_total 100000.00
    subtotal 1000.00
    subtotal_with_discount 700.00
    discount 300
    shipping true
    shipping_amount 250
    customer_email 'test@yahoo.com'
    trait :invalid_status do
      status nil
    end
    trait :invalid_grand_total do
      grand_total nil
    end
    trait :invalid_subtotal do
      subtotal nil
    end
    trait :invalid_discount do
      discount nil
    end
    trait :invalid_shipping do
      shipping nil
    end
    trait :invalid_customer_email do
      customer_email 'email'
    end
    trait :invalid_shipping_amount do
      shipping_amount nil
    end
  end
end
