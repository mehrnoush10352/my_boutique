require 'rails_helper'

context BoutiqueConfig, type: :model do
  it 'create valid config' do
    config = FactoryBot.build(:boutique_config)
    config.should be_valid
  end
  it 'empty name' do
    config = FactoryBot.build(:boutique_config, :empty_name)
    config.should be_invalid
  end
  it 'invalid values format' do
    config = FactoryBot.build(:boutique_config, :invalid_values)
    config.should be_invalid
  end
end
