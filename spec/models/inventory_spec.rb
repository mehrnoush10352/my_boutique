require 'rails_helper'

RSpec.describe Inventory, type: :model do
  it 'valid inventory' do
    inventory = FactoryBot.build(:inventory)
    inventory.should be_valid
  end
  it 'invalid count' do
    inventory = FactoryBot.build(:inventory, :invalid_count)
    inventory.should be_invalid
  end
  it 'invalid status' do
    inventory = FactoryBot.build(:inventory, :invalid_status)
    inventory.should be_invalid
  end
end
