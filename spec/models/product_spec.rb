require 'rails_helper'

context Product, type: :model do
  it 'Valid Product' do
    product = FactoryBot.build(:product)
    product.should be_valid
  end
  it 'Invalid Title' do
    product = FactoryBot.build(:product, :invalid_title)
    product.should be_invalid
  end
  it 'Invalid IS_active' do
    product = FactoryBot.build(:product, :invalid_is_active)
    product.should be_invalid
  end
  it 'Invalid Price' do
    product = FactoryBot.build(:product, :invalid_price)
    product.should be_invalid
  end
  it 'Invalid Discount' do
    product = FactoryBot.build(:product, :invalid_discount)
    product.should be_invalid
  end
  it 'Invalid Features' do
    product = FactoryBot.build(:product, :invalid_features)
    product.should be_invalid
  end
end
