require 'rails_helper'

context Customer, type: :model do
  it 'Valid Customer' do
    customer = FactoryBot.build(:customer)
    customer.should be_valid
  end
  it 'Invalid Username' do
    customer = FactoryBot.build(:customer, :invalid_username)
    customer.should be_invalid
  end
  it 'Empty Email' do
    customer = FactoryBot.build(:customer, :empty_email)
    customer.should be_invalid
  end
  it 'Invalid Email' do
    customer = FactoryBot.build(:customer, :invalid_email)
    customer.should be_invalid
  end
  it 'Empty Password' do
    customer = FactoryBot.build(:customer, :empty_password)
    customer.should be_invalid
  end
  it 'Invalid Password' do
    customer = FactoryBot.build(:customer, :invalid_password)
    customer.should be_invalid
  end
  it 'Invalid Phone Number' do
    customer = FactoryBot.build(:customer, :invalid_phone_number)
    customer.should be_invalid
  end
  it 'Duplicate Username' do
    customer1 = FactoryBot.create(:customer)
    customer2 = customer1.dup
    customer2.email = 'test@yahoo.com'
    customer2.should_not be_valid
  end
  it 'Duplicate Email' do
    customer1 = FactoryBot.create(:customer)
    customer2 = customer1.dup
    customer2.username = 'test'
    customer2.should_not be_valid
  end

end
