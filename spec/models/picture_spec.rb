require 'rails_helper'

context Picture, type: :model do
  it 'Valid Picture' do
    picture = FactoryBot.build(:picture)
    picture.should be_valid
  end
  it 'Invalid Title' do
    picture = FactoryBot.build(:picture, :invalid_title)
    picture.should be_invalid
  end
  it 'Invalid Enable' do
    picture = FactoryBot.build(:picture, :invalid_enable)
    picture.should be_invalid
  end
end
