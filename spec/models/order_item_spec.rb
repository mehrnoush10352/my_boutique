require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  it 'Valid order item' do
    item = FactoryBot.build(:order_item)
    item.should be_valid
  end
#  it 'Empty qty' do
#    item = FactoryBot.build(:order_item, :empty_qty)
#    item.should be_invalid
#  end
#  it 'Invalid qty' do
#    item = FactoryBot.build(:order_item, :invalid_qty)
#    item.should be_invalid
#  end
#  it 'Empty price' do
#    item = FactoryBot.build(:order_item, :emtpy_price)
#    item.should be_invalid
#  end
#  it 'Empty total price' do
#    item = FactoryBot.build(:order_item, :empty_total_price)
#    item.should be_invalid
#  end
#  it 'Invalid total price' do
#    item = FactoryBot.build(:order_item, :invalid_total_price)
#    item.should be_invalid
#  end
#  it 'Invalid shipping' do
#    item = FactoryBot.build(:order_item, :invalid_shipping)
#    item.should be_invalid
#  end
end
