require 'rails_helper'

context Order, type: :model do
  it 'Valid Order' do
    order = FactoryBot.build(:order)
    order.should be_valid
  end
  it 'Invalid status' do
    order = FactoryBot.build(:order, :invalid_status)
    order.should be_invalid
  end
  it 'Invalid grand_total' do
    order = FactoryBot.build(:order, :invalid_grand_total)
    order.should be_invalid
  end
  it 'Invalid subtotal' do
    order = FactoryBot.build(:order, :invalid_subtotal)
    order.should be_invalid
  end
  it 'Invalid discount' do
    order = FactoryBot.build(:order, :invalid_discount)
    order.should be_invalid
  end
  it 'Invalid shipping' do
    order = FactoryBot.build(:order, :invalid_shipping)
    order.should be_invalid
  end
  it 'Invalid customer email' do
    order = FactoryBot.build(:order, :invalid_customer_email)
    order.should be_invalid
  end
  it 'Invalid shipping amount' do
    order = FactoryBot.build(:order, :invalid_shipping_amount)
    order.should be_invalid
  end
end
