require 'rails_helper'

context Category, type: :model do
  it 'Valid category' do
    category = FactoryBot.build(:category)
    category.should be_valid
  end
  it 'Invalid title' do
    category = FactoryBot.build(:category, :invalid_title)
    category.should be_invalid
  end
  it 'Invalid is_active' do
    category = FactoryBot.build(:category, :invalid_is_active)
    category.should be_invalid
  end
end
