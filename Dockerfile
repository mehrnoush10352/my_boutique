FROM ruby:2.4.2

MAINTAINER Mehrnoush Akbari "akbari10352@gmail.com"

RUN curl --silent --location https://deb.nodesource.com/setup_6.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt-get install -y apt-utils
RUN apt-get install -qq --no-install-recommends -y curl build-essential libpq-dev nodejs yarn
RUN mkdir -p /usr/local/yarn

COPY package.json /usr/local/yarn/package.json
COPY yarn.lock /usr/local/yarn/yarn.lock
RUN cd /usr/local/yarn && yarn


RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
RUN gem install bundler
RUN bundle install
Add . /app
