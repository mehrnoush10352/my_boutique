sidekiq_config = Rails.application.config_for :sidekiq

Sidekiq.configure_server do |config|
  config.average_scheduled_poll_interval = 3
  config.redis = {
    url: "redis://#{sidekiq_config[:host]}:#{sidekiq_config[:port]}/#{sidekiq_config[:database]}"
  }
end
Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{sidekiq_config[:host]}:#{sidekiq_config[:port]}/#{sidekiq_config[:database]}" }
end
