require 'rake'
require 'elasticsearch/rails/tasks/import'
namespace :elasticsearch do
  desc 'create class for elastic'
  task :create, [:class_name] do |task, args|
    klass = args[:class_name]
    puts "creating #{klass} index..."
    klass.constantize.__elasticsearch__.create_index!
    puts 'done!!'
  end

  # rake environment elasticsearch:import:model CLASS='class name'
end
